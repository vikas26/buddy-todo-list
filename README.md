# Buddy To-do list - This is a 8 hour test done for a firm

A demo web app using PHP (CakePHP), mysql and Materialzie UI that does following things.

- This is a to-do list with a twist. In this application, the to-do for the buddy which a user selects at the start.
- At the time of creating a to-do list, a user is supposed to give the email address of the buddy along with the name of the list. Only those who are in the system can be added as a buddy. List is created only if a buddy can be added.
- After the list is created, the buddy list system works like if the user adds a task, it goes on their buddy's list and vice versa. Once the buddy completes a list item, it is shown on the user's list as completed and is not shown on the buddy's list.