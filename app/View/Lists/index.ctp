<!-- created for me -->
<div class="row center">
    <h4>Created for me:</h4>
</div>

<div class="row">
    <?php foreach ($data['lists']['listsForMe'] as $listKey => $list) { ?>
        <div class="my-list">
            <h6><?php echo $list['data']['title']; ?></h6>
            <ul class="my-lists-items collection">
                <?php foreach ($list['items'] as $itemKey => $item) { ?>
                    <li class="collection-item"
                        data-id="<?php echo $item['id']; ?>">
                        <?php echo $item['title']; ?>
                        <span class="btn primary mark-as-completed right">
                            <i class="material-icons">check</i>
                        </span>
                    </li>
                <?php } ?>
            </ul>
        </div>
    <?php } ?>
</div>

<!-- created by me -->
<div class="row center">
    <h4>Created by me:</h4>
</div>

<div class="row">
    <?php foreach ($data['lists']['listsByMe'] as $listKey => $list) { ?>
        <div class="my-list">
            <h6><?php echo $list['data']['title']; ?></h6>
            <ul class="my-lists-items collection">
                <?php foreach ($list['items'] as $itemKey => $item) { ?>
                    <li class="collection-item">
                        <?php echo $item['title']; ?>
                    </li>
                <?php } ?>
            </ul>
        </div>
    <?php } ?>
</div>