<div class="row">
    <h4>Buddy List:</h4>
</div>

<div class="row">
    <form name="create_buddy_list_form" class="col s12">

        <div class="row">
            <div class="input-field col s12">
                <input id="email" type="email">
                <label for="email">Buddy Email</label>
            </div>
        </div>
        
        <div class="row">
            <div class="input-field col s12">
                <input id="title" type="text">
                <label for="title">List Title</label>
            </div>
        </div>

        <div class="row">
            <h6>To do items:</h6>
        </div>

        <div class="row">
            <table class="list-item-table">
                <tbody>
                    <tr class="remove-row-template main">
                        <td>
                            <input type="text" disabled/>
                        </td>
                        <td>
                            <span class="btn red remove-row">
                                <i class="material-icons">remove</i>
                            </span>
                        </td>
                    </tr>

                    <tr class="add-row-template">
                        <td>
                            <input type="text"/>
                        </td>
                        <td>
                            <span class="btn primary add-row">
                                <i class="material-icons">add</i>
                            </span>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <button class="btn primary" type="submit">Save</button>
    </form>
</div>