<html lang="en">
    <head>
    <?php echo $this->Html->charset(); ?>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="theme-color" content="#154579" />
        <title>
        <?php echo $data['title']; ?>
        </title>
        
        <!--Import Google Icon Font-->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <?php
        if (ENV == 'local') {
            // local
            echo $this->Html->css(array(
                'materialize.min.css',
                'common.css?version='.VERSION,
            ));
            echo $this->Html->script(array(
                'jquery-2.1.1.js',
            ));
        } else {
            // prod
        }

        switch ($data['pagetitle']) {
            case 'create':
                echo $this->Html->css(array(
                    'create.css?version='.VERSION,
                ));
                break;
            case 'lists':
                echo $this->Html->css(array(
                    'lists.css?version='.VERSION,
                ));
                break;
            default:
                break;
        }

        echo $this->fetch('meta');
        echo $this->fetch('css');
    ?>

    <script type="text/javascript">
        const SITE_URL = "<?php echo SITE_URL; ?>";
    </script>
    
    </head>
    <body>

    <header class="teal lighten-2">
        <a href="#" data-activates="slide-out" class="button-collapse"><i class="material-icons">menu</i></a>

        <?php if ($data['pagetitle'] != 'login') { ?>
            <a class="white-text logout-btn" href='<?php echo SITE_URL; ?>users/logout'>Logout</a>
        <?php } ?>
    </header>

    <div>
        <ul id="slide-out" class="side-nav fixed">
            <li class="<?php echo $data['pagetitle'] == 'create' ? 'active' : ''; ?>">
                <a href="<?php echo SITE_URL; ?>lists/create">Create</a>
            </li>
            <li class="<?php echo $data['pagetitle'] == 'lists' ? 'active' : ''; ?>">
                <a href="<?php echo SITE_URL; ?>lists/">Lists</a>
            </li>
        </ul>
    </div>

    <main>
        <?php echo $this->fetch('content'); ?>
    </main>

    <?php
        if (ENV == 'local') {
            echo $this->Html->script(array(
                'materialize.min.js',
                'common.js?version='.VERSION,
            ));
        } else {
            // prod
        }

        switch ($data['pagetitle']) {
            case 'create':
                echo $this->Html->script(array(
                    'create.js?version='.VERSION,
                ));
                break;
            case 'lists':
                echo $this->Html->script(array(
                    'lists.js?version='.VERSION,
                ));
                break;
            case 'login':
                echo $this->Html->script(array(
                    'login.js?version='.VERSION,
                ));
                break;
            default:
                break;
        }
        echo $this->fetch('script');
    ?>
    
    </body>
</html>
