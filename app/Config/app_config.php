<?php

define('ENV', 'local'); // local or live

if (ENV == 'local') {
    define('DB_HOST', 'localhost');
    define('DB_USER', 'root');
    define('DB_PASS', '');
    define('DB_NAME', 'buddy-todo');
    define('SITE_URL', 'http://'.$_SERVER['HTTP_HOST'].'/buddy-todo/');
} else {
}
