<?php

class ToDoList extends AppModel {

    public $useTable = 'lists';

    // get my to do lists
    // input - email address
    // returns json brand default template
    // called by Lists controller - index
    public function myLists ($email) {

        $data = array();

        // created for me
        $data['listsForMe'] = $this->_createdForMe($email);

        // created by me
        $data['listsByMe'] = $this->_createdbyMe($email);

        return $data;
    }

    // lists created for me
    function _createdForMe($email) {
        $formattedData = array();
        
        // data
        $data = $this->find('all', 
            array(
                'fields' => array(
                    'ToDoList.id',
                    'ToDoList.title',
                    'ToDoItem.id',
                    'ToDoItem.title',
                    'ToDoItem.is_completed',
                ),
                'conditions' => array(
                    'User.email' => $email,
                    'ToDoItem.is_completed' => "no",
                ),
                'joins' => array(
                    array(
                        'table' => 'users',
                        'alias' => 'User',
                        'type' => 'inner',
                        'conditions' => array(
                            'User.id = ToDoList.buddy_id',
                        )
                    ),
                    array(
                        'table' => 'todo_items',
                        'alias' => 'ToDoItem',
                        'type' => 'inner',
                        'conditions' => array(
                            'ToDoItem.list_id = ToDoList.id',
                        )
                    )
                )
            )
        );
        // format data
        foreach ($data as $key => $value) {
            $list_id = $value['ToDoList']['id'];

            if (!array_key_exists($list_id, $formattedData)) {
                $formattedData[$list_id] = array(
                    'data' => $value['ToDoList'],
                    'items' => array()
                );
            }

            array_push($formattedData[$list_id]['items'], $value['ToDoItem']);
        }

        return $formattedData;
    }

    // lists created by me
    function _createdByMe($email) {
        $formattedData = array();
        
        // data
        $data = $this->find('all', 
            array(
                'fields' => array(
                    'ToDoList.id',
                    'ToDoList.title',
                    'ToDoItem.id',
                    'ToDoItem.title',
                    'ToDoItem.is_completed',
                ),
                'conditions' => array(
                    'User.email' => $email,
                    'ToDoItem.is_completed' => "yes",
                ),
                'joins' => array(
                    array(
                        'table' => 'users',
                        'alias' => 'User',
                        'type' => 'inner',
                        'conditions' => array(
                            'User.id = ToDoList.created_by',
                        )
                    ),
                    array(
                        'table' => 'todo_items',
                        'alias' => 'ToDoItem',
                        'type' => 'inner',
                        'conditions' => array(
                            'ToDoItem.list_id = ToDoList.id',
                        )
                    )
                )
            )
        );
        // format data
        foreach ($data as $key => $value) {
            $list_id = $value['ToDoList']['id'];

            if (!array_key_exists($list_id, $formattedData)) {
                $formattedData[$list_id] = array(
                    'data' => $value['ToDoList'],
                    'items' => array()
                );
            }

            array_push($formattedData[$list_id]['items'], $value['ToDoItem']);
        }

        return $formattedData;
    }
}