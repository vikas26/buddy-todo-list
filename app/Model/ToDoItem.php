<?php

class ToDoItem extends AppModel {

    public $useTable = 'todo_items';

    // mark as completed
    function markAsCompleted($itemId) {
        $this->updateAll(
            array('ToDoItem.is_completed' => '"yes"'),
            array('ToDoItem.id' => $itemId)
        );
    }
}