<?php

class User extends AppModel {

    public $useTable = 'users';

    // search users by email
    // input - email address
    // returns json brand default template
    // called by users controller - search
    public function searchByEmail ($userEmail, $limit=10) {

        return $this->find('all', 
            array(
                'fields' => array(
                    'User.id',
                    'User.first_name',
                    'User.last_name',
                    'User.email',
                ),
                'conditions' => array(
                    'User.email like "%'.$userEmail.'%"',
                ),
            )
        );
    }

    // get user by email
    // input - email address
    // returns json brand default template
    // called by lists controller - save
    public function getUserByEmail ($userEmail) {

        return $this->find('first', 
            array(
                'fields' => array(
                    'User.id',
                    'User.first_name',
                    'User.last_name',
                    'User.email',
                ),
                'conditions' => array(
                    'User.email' => $userEmail,
                ),
            )
        );
    }
}