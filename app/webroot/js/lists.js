"use strict";

// mark as completed
let markAsCompleted = function (form_data, callback_fn) {
    $.ajax({
        type: "POST",
        url: SITE_URL + "lists/complete/",
        data: form_data,
        dataType: "application/json"
    }).always(function (response) {
        let json_response = JSON.parse(response.responseText);
        callback_fn(form_data, json_response);
    });
};

// mark as completed
let markAsCompletedCallback = function (form_data, response) {
    alert(response.message);
    if (!response.error) {
        window.location.href = SITE_URL + 'lists/';
    }
};

$(document).ready(function () {

    // login now
    $(document.body).on('click', '.mark-as-completed', function () {
        
        let itemId = this.parentElement.dataset['id']

        // save to do list
        markAsCompleted({
            'itemId': itemId,
        }, markAsCompletedCallback);

    });
});