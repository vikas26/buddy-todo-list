"use strict";

// search user by email
let searchUserByEmail = function (form_data, callback_fn) {
    $.ajax({
        type: "POST",
        url: SITE_URL + "users/search/",
        data: form_data,
        dataType: "application/json"
    }).always(function (response) {
        let json_response = JSON.parse(response.responseText);
        callback_fn(form_data, json_response);
    });
};

// search user by email callback
let searchUserByEmailCallback = function (response) {
    console.log(response);
};

// save to do list
let saveToDoList = function (form_data, callback_fn) {
    $.ajax({
        type: "POST",
        url: SITE_URL + "lists/save/",
        data: form_data,
        dataType: "application/json"
    }).always(function (response) {
        let json_response = JSON.parse(response.responseText);
        callback_fn(form_data, json_response);
    });
};

// save to do list callback
let saveToDoListCallback = function (form_data, response) {
    alert(response.message);
    if (!response.error) {
        window.location.href = SITE_URL + 'lists/';
    }
};

$(document).ready(function () {

    let lastSearchTimeout = null;

    // on change of email input
    $(document.body).on('input', '#email', function () {
        let userEmail = this.value;

        // clear timeout if any
        clearTimeout(lastSearchTimeout);
        
        // set new timeout
        lastSearchTimeout = setTimeout(function () {
            // searchUserByEmail({
            //     'email': userEmail
            // }, searchUserByEmailCallback);
        }, 200);
    });

    // on click of add item
    $("body .list-item-table").on('click', '.add-row', function () {
        let inputText   = $(this).parents("tr").find("input");
        let newRow      = $("body .list-item-table .remove-row-template.main").clone();

        newRow.removeClass("main");
        newRow.find("input").val(inputText.val());
        inputText.val('');

        $(this).parents("tbody").append(newRow);
    });

    // on click of remove item
    $("body .list-item-table").on('click', '.remove-row', function () {
        $(this).parents("tr").remove()
    });

    // create new to do list
    $(document.body).on('submit', 'form[name="create_buddy_list_form"]', function (e) {
        e.preventDefault();
        
        let email = $(this).find("#email").val();
        let title = $(this).find("#title").val();
        let items = [];

        $(this).find(".remove-row-template:not(.main)").each(function(){
            items.push($(this).find("input").val());
        });

        // save to do list
        saveToDoList({
            'email': email,
            'title': title,
            'items': items
        }, saveToDoListCallback);

    });
});