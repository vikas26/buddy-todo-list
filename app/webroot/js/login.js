"use strict";

// login
let loginNow = function (form_data, callback_fn) {
    $.ajax({
        type: "POST",
        url: SITE_URL + "users/login/",
        data: form_data,
        dataType: "application/json"
    }).always(function (response) {
        let json_response = JSON.parse(response.responseText);
        callback_fn(form_data, json_response);
    });
};

// login callback
let loginNowCallback = function (form_data, response) {
    alert(response.message);
    if (!response.error) {
        window.location.href = SITE_URL + 'lists/';
    }
};

$(document).ready(function () {

    // login now
    $(document.body).on('submit', 'form[name="login_form"]', function (e) {
        e.preventDefault();
        
        let email = $(this).find("#email").val();

        // save to do list
        loginNow({
            'email': email,
        }, loginNowCallback);

    });
});