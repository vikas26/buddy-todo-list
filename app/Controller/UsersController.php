<?php

class UsersController extends AppController{
    
    public $uses = array('User');
    
    function beforeFilter() {
        parent::beforeFilter();
    }

    // login page
    function index() {
        $data['title'] = 'Login';
        $data['pagetitle'] = 'login';
        $this->set('data', $data);
    }

    // login using email address
    function login() {
        $this->autoRender = false;

        // response data
        $data = array(
            'error' => false,
            'error_type' => '',
            'message' => 'Sucessfully logged in!',            
        );

        // collect post data
        $email = $this->request->data['email'];

        // check email address in system
        $buddyDetails = $this->User->getUserByEmail($email);

        // buddy email not found
        if (count($buddyDetails) < 1) {
            $data['error'] = true;
            $data['message'] = 'User not found!';
            return json_encode($data);
        }

        // destroy session
        $this->Session->destroy();

        // set session
        $this->Session->write (array(
            'email' => $email
        ));

        // return response
        return json_encode($data);
    }

    // logout 
    function logout() {
        $this->autoRender = false;
        $this->checkUserAccess();
        $this->Session->destroy();
        $this->redirect(SITE_URL);
    }

    // search user by email
    function search() {
        $this->autoRender = false;

        // check user access
        $this->checkUserAccess();

        // response data
        $data = array(
            'error' => false,
            'error_type' => '',
            'results' => array(),
            'resultsCount' => 0
        );

        // get post data
        $userEmail = $this->request->data['email'];

        // if useremail has ome value
        if (strlen($userEmail) > 0) {
            // get user by email
            $usersMatched           = $this->User->searchByEmail ($userEmail);
            $data['results']        = $usersMatched;
            $data['resultsCount']   = count($usersMatched);
        }

        // return response
        return json_encode($data);
    }
}
