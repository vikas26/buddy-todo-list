<?php

class ListsController extends AppController{
    
    public $uses = array('User', 'ToDoList', 'ToDoItem');
    
    function beforeFilter() {
        parent::beforeFilter();
    }

    // show user lists
    function index() {
        // check user access
        $this->checkUserAccess();

        // email address
        $email = $this->Session->read('email');

        $data               = array();
        $data['title']      = 'All list';
        $data['pagetitle']  = 'lists';
        $data['lists']      = $this->ToDoList->myLists($email);
        $this->set('data', $data);
    }

    // create new list
    function create() {
        // check user access
        $this->checkUserAccess();

        $data['title'] = 'New list';
        $data['pagetitle'] = 'create';
        $this->set('data', $data);
    }

    // save to do list
    function save() {
        $this->autoRender = false;

        // check user access
        $this->checkUserAccess();

        // response data
        $data = array(
            'error' => false,
            'error_type' => '',
            'message' => 'Saved sucessfully',            
        );

        // get post data
        $email = $this->request->data['email'];
        $title = $this->request->data['title'];
        $items = $this->request->data['items'];

        // check buddy email address
        $buddyDetails = $this->User->getUserByEmail($email);

        // get my details
        $myEmail    = $this->Session->read('email');
        $myDetails  = $this->User->getUserByEmail($myEmail);

        // buddy email not found
        if (count($buddyDetails) < 1) {
            $data['error'] = true;
            $data['message'] = 'Buddy not found!';
            return json_encode($data);
        }

        // save new list
        $this->ToDoList->save(array(
            'created_by'    => $myDetails['User']['id'],
            'buddy_id'      => $buddyDetails['User']['id'],
            'title'         => $title
        ));

        // list id
        $list_id = $this->ToDoList->getLastInsertID();

        // items to save array
        $itemsTosave = array();

        // items to save
        foreach ($items as $key => $value) {
            array_push($itemsTosave, array(
                'list_id' => $list_id,
                'title' => $value,
            ));
        }

        // save all list items
        $this->ToDoItem->saveAll($itemsTosave);

        // return response
        return json_encode($data);
    }

    // complete list item
    function complete() {
        $this->autoRender = false;

        // response data
        $data = array(
            'error' => false,
            'error_type' => '',
            'message' => 'Updated sucessfully',            
        );

        // check user access
        $this->checkUserAccess();

        // get post data
        $itemId = $this->request->data['itemId'];

        // mark item as completed
        $this->ToDoItem->markAsCompleted($itemId);

        // return response
        return json_encode($data);
    }
}
